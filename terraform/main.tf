terraform {
  backend "http" {}
}

module "my_module_name" {
  source = "gitlab.com/dependabot-gitlab/gitlab-file/local"
  version = "0.0.3"
}

output "filesize_in_bytes" {
  value = module.my_module_name.bytes
}
